<h1>EP2</h1>
<h2>DESCRIÇÃO</h2>
<p>Programa Java com biblioteca Swing para aplicar filtros em imagens PPM e PGM.</p>
<h2>PRÉ REQUISITOS</h2>
<p> Fazer do donwnload do projeto, abrir no NetBeans e executar.</p>
<h2>USO</h2>
<p>Ao iniciar o programa, clicar no botão para o tipo de imagem a ser tratada e escolher um arquivo desse tipo para subir.</p>
<p>Se a imagem for válida, ela será carregada para visualização e uma mensagem de "imagem aberta com sucesso!" aparecerá.</p>
<p>Então, clicar nos botões dos filtros correspontendes para cada tipo de imagem.</p>
<p>A imagem pode ser trocada a qualquer momento, para a aplicação dos filtros em outras imagens, sem a necessidade de reiniciar o programa.</p>