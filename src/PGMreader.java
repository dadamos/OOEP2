
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dadamos
 */
public class PGMreader {
    public static String le_linha(FileInputStream arquivo) {
        String linha = "";
        byte bb;
        try {
                while ((bb = (byte) arquivo.read()) != '\n') {
                        linha += (char) bb;
                }
        } catch (IOException e) {
                e.printStackTrace();
        }
        //System.out.println("Linha: " + linha);
        return linha;
    }
}